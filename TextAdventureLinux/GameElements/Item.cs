﻿using System;

using GameSystem;
using Enums;

namespace GameElements
{
	// General Items
	public class Item : Base
	{
		private string _place;
		private bool _isPickable;
		private bool _isUsable;

		public string Place
		{
			get{ return _place; }
			set{ _place = value; }
		}
		public bool IsPickable
		{
			get{ return _isPickable; }
			set{ _isPickable = value; }
		}
		public bool IsUsable
		{
			get{ return _isUsable; }
			set{ _isUsable = value; }
		}

		public Item(bool isPickable, bool isUsable, string name, string description, string place) : base(name, description)
		{
			IsPickable = isPickable;
			IsUsable = IsUsable;
			Place = place;
		}
	}
	/*
	// Doors
	public class Door : Base
	{
		private bool _isLocked;
		private Direction _exit;

		public bool IsLocked
		{
			get{ return _isLocked; }
			set{ _isLocked = value; }
		}

		public Direction Exit
		{
			get { return _exit; }
		}

		public Door(string name, string description, bool isLocked, Direction exit) : base(name, description)
		{
			IsLocked = isLocked;
		}
	}
	*/
}
